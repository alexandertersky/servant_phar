<?php

namespace Bin;

use Src\Console;

class App
{
    private $args;
    private $cmd;

    public function __construct($args)
    {
        $this->args = array_splice($args, 1);
    }

    public function run()
    {
        $this->cmd = $this->args[0];
        $this->checkCmd();
    }

    private function checkCmd()
    {
        if (!method_exists($this, $this->cmd)){
            Console::error('Undefined command \''. $this->cmd.'\''.PHP_EOL.'Use command \'help\'');
            exit();
        }
    }

}