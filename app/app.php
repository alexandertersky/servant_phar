<?php

require_once __DIR__.'/vendor/autoload.php';

use Bin\App;

(new App($argv))->run();