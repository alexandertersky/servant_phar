<?php

namespace Src;

class Console
{
    public static function error($msg)
    {
        self::write($msg, "\033[41m");
    }

    private static function write($msg, $background)
    {
        $parts = explode(PHP_EOL, $msg);
        $length = self::getLength($parts);

        self::line($length, $background);

        foreach ($parts as $part){
            echo $background;
            $msg = ' ' . $part . ' ';
            echo $msg;
            echo str_repeat(' ', $length - mb_strlen($msg)+2);
            self::clear();
            echo PHP_EOL;
        }

        self::line($length, $background);
        self::clear();
    }

    private static function line($length, $bg)
    {
        echo $bg;
        $top = '';
        for ($i = 0; $i < $length + 2; $i++) {
            $top .= ' ';
        }
        echo $top;
        self::clear();
        echo PHP_EOL;
    }

    private static function getLength($parts)
    {
        $l = 0;

        foreach ($parts as $part) {
            $len = mb_strlen($part);
            if ($len > $l) {
                $l = $len;
            }
        }

        return $l;
    }

    private static function clear()
    {
        echo "\033[0m";
    }
}